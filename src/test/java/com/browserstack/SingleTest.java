package com.browserstack;
import com.browserstack.local.Local;

//import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.WebElement;

import org.testng.annotations.Test;
import org.testng.Assert;


import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.*;

import java.util.*;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.WrapsDriver;


import java.net.URL;

import com.codeborne.selenide.WebDriverRunner;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Parameters;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import java.util.concurrent.locks.Condition;


public class SingleTest  {

  public static final String USERNAME = "gerritleder1";
  public static final String AUTOMATE_KEY = "cSHT1qFJ9Gzff472ECfE";
  public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

	public RemoteWebDriver driver;
	private Local l;
	private int timeOut = 5;

	@BeforeMethod(alwaysRun=true)
	@Parameters(value={"config", "environment"})
  public void setUp(String config_file, String environment) throws java.net.MalformedURLException {

        DesiredCapabilities caps = new DesiredCapabilities();
    	caps.setCapability("browser", "Chrome");
    	caps.setCapability("browser_version", "80.0 beta");
    	caps.setCapability("os", "Windows");
    	caps.setCapability("os_version", "10");
    	caps.setCapability("resolution", "1024x768");
    	caps.setCapability("name", "gmr_selenide");
	caps.setCapability("projectName", "gmr_selenide");
	caps.setCapability("buildName", "Build 2.0");
	caps.setCapability("sessionName", "click all links");


    //	try {
   
      		driver = new RemoteWebDriver(new URL(URL), caps);
		WebDriverRunner.setWebDriver(driver);

  //  	} catch (java.net.MalformedURLException ex) {
    //  		System.out.println (ex);
    //	}

  }

	@Test
	public void test_open() throws Exception {

		open("https://infinite-taiga-25466.herokuapp.com/");

		



		//$("title").shouldHave(text("Blog"));

//		Assert.assertEquals(title(), "Blog");

	}


		

	@Test
	public void test_click_all_links() throws Exception {
		
		open("https://infinite-taiga-25466.herokuapp.com/");

		//get all links with href that start with https://
		ArrayList<String> links = (ArrayList) ((JavascriptExecutor) driver).executeScript("return [...document.querySelectorAll(\"a[href^='https://']\")].map(e=>e.getAttribute('href'))");

		links.forEach(link->{
			System.out.println(link);//DEBUG
			open(link);
		    //check here
			$(withText("The page you were looking for")).shouldNot(exist);
		//	Assert.assertNotEquals(title(), "The page you were looking for doesn't exist.");


			//get all sublinks with href that start with https://
			ArrayList<String> sublinks = (ArrayList) ((JavascriptExecutor) driver).executeScript("return [...document.querySelectorAll(\"a[href^='https://']\")].map(e=>e.getAttribute('href'))");

			

			sublinks.forEach(link2->{
				System.out.println(link2);//DEBUG			
				open(link2);
		    //check here
			$(withText("The page you were looking for")).shouldNot(exist);
//				Assert.assertNotEquals(title(), "The page you were looking for doesn't exist.");


			});

		});

	}


	@AfterMethod(alwaysRun=true)
	public void tearDown() throws Exception {
		driver.quit();
		if (l != null) l.stop();
	}

}
